using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.PaymentApp.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Security.Principal;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Infosys.EduBank.PaymentApp.Controllers
{
    public class RedirectToMerchantController : Controller
    {
        public IActionResult Success()
        {
            if (HttpContext.Session.GetString("UserToken")==null){
                return RedirectToAction("Error","RedirectToMerchant");
            }
            ViewData["UserId"] = HttpContext.Session.GetString("UserId");
            ViewData["ReturnURL"] =  HttpContext.Session.GetString("ReturnURL");

            //HttpContext.Session.Clear();
            ViewData["Message"] = "Your payment was successful. You will be redirected soon to the merchant app.";

            return View();
        }

        public IActionResult Failure()
        {
            if (HttpContext.Session.GetString("UserToken")==null){
                return RedirectToAction("Error","RedirectToMerchant");
            }
            ViewData["UserId"] = HttpContext.Session.GetString("UserId");
            ViewData["ReturnURL"] =  HttpContext.Session.GetString("ReturnURL");
            
            //HttpContext.Session.Clear();
            ViewData["Message"] = "Your payment failed. You will be redirected soon to the merchant app.";

            return View();
        }

        public IActionResult Error()
        {
            ViewData["UserId"] = HttpContext.Session.GetString("UserId");
            ViewData["ReturnURL"] =  HttpContext.Session.GetString("ReturnURL");
            
            //HttpContext.Session.Clear();
            ViewData["Message"] = "Some error occured. If money was deducted from your account, It shall be reverted within 7 working days. You will be redirected soon to the merchant app.";

            return View();
        }

        public IActionResult RedirectToMerchant(string status)
        {
            var redirectUrl = HttpContext.Session.GetString("ReturnURL");
            HttpContext.Session.Clear();
            return Redirect(redirectUrl+status);
        }
    }
}