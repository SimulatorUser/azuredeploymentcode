using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.PaymentApp.Models;
using Infosys.EduBank.Common.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Security.Principal;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using Microsoft.Extensions.Configuration;

namespace Infosys.EduBank.PaymentApp.Controllers
{
    public class NetBankingPaymentController : Controller
    {
        public HttpClient httpClient { get; set; }
        public NetBankingPaymentController()
        {
            httpClient = new HttpClient();
        }

        public IActionResult NetBankingLogin(string appId,string amount,string userId,string errorMessage="")
        {
            try{
                    ViewData["errorMessage"] = errorMessage;

                    if (HttpContext.Session.GetString("UserToken")!=null){
                        return RedirectToAction("NetBankingPayment","NetBankingPayment");
                    }

                appId = DecryptForInputParams(appId,true);
                amount = DecryptForInputParams(amount,true);
                userId = DecryptForInputParams(userId,true);

                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");
                var setting = builder.Build();


                HttpContext.Session.SetString("UserId", userId);
                HttpContext.Session.SetString("ReturnURL",setting["RegisteredMerchants:"+appId+":MerchantReturnUrl"]);
                HttpContext.Session.SetString("Amount",amount.ToString());
                return View();
            }
            catch(Exception ex){
                return RedirectToAction("Error","RedirectToMerchant");
            } 
        }

        [HttpPost]
        public IActionResult NetBankingLogin(Models.InternetBankingTransaction internetBankingTransaction)
        {
            try{
                User user = new User(){
                UserName = internetBankingTransaction.LoginName,
                Password = internetBankingTransaction.Password
                };

                #region Call Authentication SL
                string URL = "https://edubankauthentication.azurewebsites.net/api/authentication/";

               //string requestBody = JsonConvert.SerializeObject(user);
                
                UserAuth userAuth = new UserAuth();
                var response = httpClient.PostAsJsonAsync(URL,user).Result;
                if (response.IsSuccessStatusCode){
                    var  result=response.Content.ReadAsStringAsync().Result;
                    if(!string.IsNullOrEmpty(result)){
                         userAuth= JsonConvert.DeserializeObject<Common.Models.UserAuth>(result);
                        HttpContext.Session.SetString("UserToken",userAuth.Token);
                        }
                }
                //var userAuth = response.Result.Content.ReadAsAsync<Common.Models.UserAuth>().Result;

                #endregion

                

                // debugging purpose
                // var userAuth = new UserAuth(){
                //     Id = 123456,
                //     Name = "Neha Thakur",
                //     Token = "askjfkjsbfkjablahblah",
                //     Role = "Customer"
                // };

                if (userAuth.Token !=null){
                    HttpContext.Session.SetString("UserToken",userAuth.Token);
                    HttpContext.Session.SetString("AccountHolderName", userAuth.Name);
                    HttpContext.Session.SetString("UserName",user.UserName);
                    return RedirectToAction("NetBankingPayment","NetBankingPayment"); //pass control to MakePayment Page
                }

                return NetBankingLogin(HttpContext.Session.GetString("UserId"),HttpContext.Session.GetString("Amount"),"Invalid Credentials. Please Try again.");
            }
            catch(Exception ex){
                return RedirectToAction("Error","RedirectToMerchant");
            }
        }

        public IActionResult NetBankingPayment()
        {
            try{

                if (HttpContext.Session.GetString("UserToken")!=null){
                
                 #region AccountNumbersAPIHit
                string URL = "https://edubankuserservice.azurewebsites.net/api/getaccountnumbers?userName=" + HttpContext.Session.GetString("UserName");

                 httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",HttpContext.Session.GetString("UserToken"));
                var response = httpClient.GetAsync(URL);
                

                List<string> accountNumbers = response.Result.Content.ReadAsAsync<List<string>>().Result;
                 #endregion

                ViewData["AccountNumbers"] = accountNumbers;
                //ViewData["AccountNumbers"] = new List<String>(){ "123456789012","12345678910","2742873427843"};
                return View(); 
                }

                return RedirectToAction("Error","RedirectToMerchant");

            }
            catch(Exception ex){
                return RedirectToAction("Error","RedirectToMerchant");
            }  
        }

        [HttpPost]
        public IActionResult NetBankingPaymentPost(IFormCollection formCollection)
        {
            try{
                //return RedirectToAction("Error","RedirectToMerchant"); //debugging purpose

                if (HttpContext.Session.GetString("UserToken")!=null){

                    #region DebitUsingNetBanking API Call

                    string URL = "https://edubanknetbankingservice.azurewebsites.net/api/netbanking/";
                    Common.Models.NetBankingDetails netBankingDetails = new Common.Models.NetBankingDetails(){
                        AccountNumber = formCollection["accountnumber"].ToString(),
                        IFSC = string.Empty,
                        Remarks = formCollection["remarks"].ToString(),
                        AccountHolderName = HttpContext.Session.GetString("AccountHolderName"),
                        Amount = Convert.ToDecimal(HttpContext.Session.GetString("Amount"))
                    };

                   // string requestBody = JsonConvert.SerializeObject(netBankingDetails);

                    // // Adding Token to header
                    //var tokenBytes = Encoding.ASCII.GetBytes(HttpContext.Session.GetString("UserToken"));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",HttpContext.Session.GetString("UserToken"));
                    httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

                    var response = httpClient.PostAsJsonAsync(URL,netBankingDetails);
                    var result = response.Result.Content.ReadAsAsync<string>().Result;
                    
                    #endregion
                    
                    //var result = "SUCCESS"; //debugging purpose
                    if (result.Equals("SUCCESS")){
                        // var responseForConfirmation = httpClient.PostAsJsonAsync(HttpContext.Session.GetString("TransactionURL"),new{ userid = HttpContext.Session.GetString("UserId"), amount = HttpContext.Session.GetString("Amount")});
                        // if (responseForConfirmation.Result.IsSuccessStatusCode)
                        return RedirectToAction("Success","RedirectToMerchant");
                    }
                }

                return RedirectToAction("Failure","RedirectToMerchant");

            }
            catch(Exception ex){

                return RedirectToAction("Error","RedirectToMerchant");
            }

        }

        public string DecryptForInputParams(string toDecrypt, Boolean isNum){
            try{
                var specialChars = "1@euTp9!mzXrxP]~tnSl{|4jw5}[";
                //toDecrypt = Microsoft.AspNetCore.WebUtilities.WebEncoders.Base64UrlDecode(toDecrypt).ToString(); // replace url chars
                var decryptedData = string.Empty;
                int position = 0;
                foreach(var item in toDecrypt){
                    position = specialChars.IndexOf(item);
                    if (position>=0 && position<26){
                        if(isNum){
                            decryptedData+=position;
                        }
                        else{
                            decryptedData+= (char)(65 + position);
                        }
                        for(int i=0;i<position;i++){
                            specialChars = specialChars.Substring(1) + specialChars[0];
                        }
                    }
                }
                return decryptedData;
            }
            catch(Exception ex){
                return null;
            }
        }
    }
}
