using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.PaymentApp.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Security.Principal;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace Infosys.EduBank.PaymentApp.Controllers
{
    public class DebitCardPaymentController : Controller
    {
        public HttpClient httpClient { get; set; }

        //public DebitCardTransaction DummyDebitCardTransaction { get; set; }
        public DebitCardPaymentController()
        {
            httpClient = new HttpClient();
            // DummyDebitCardTransaction = new DebitCardTransaction(){
            //     DebitCard = new DebitCardDetails{
            //         DebitCardNumber = "5166020070660839",
            //         CVV = "072",
            //         ValidThru = "11/28",
            //         CardHolderName = "Harsh"
            //     },
            //     Amount = 666,
            //     Remarks = "Amigo Wallet Debit",
            //     MerchantName = "Amigo Wallet",
            //     MerchantUserId = "srinidhi.01",
            //     MerchantReturnURL = "https://www.google.com/maps/"
            // };

        }

        public IActionResult DebitCardPayment(string appId,string userId, string debitCardNumber,string cardHolderName,string validThru,string cvv,string amount)
        {
            try{

                appId = DecryptForInputParams(appId,true);
                userId = DecryptForInputParams(userId,true);
                debitCardNumber = DecryptForInputParams(debitCardNumber,true);
                cardHolderName = DecryptForInputParams(cardHolderName,false);
                validThru = DecryptForInputParams(validThru,true);
                cvv = DecryptForInputParams(cvv,true);
                amount = DecryptForInputParams(amount, true);
                
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");
                var setting = builder.Build();
                 
                
                var debitCardTransaction = new DebitCardTransaction(){
                DebitCard = new DebitCardDetails{
                    DebitCardNumber = debitCardNumber,
                    CVV = cvv,
                    ValidThru = validThru.Substring(0,2)+"/"+validThru.Substring(4,2),
                    CardHolderName = cardHolderName
                },
                Amount = Convert.ToDecimal(amount),
                Remarks = setting["RegisteredMerchants:"+appId+":MerchantName"],
                MerchantName = setting["RegisteredMerchants:"+appId+":MerchantName"],
                MerchantUserId = userId,
                MerchantReturnURL = setting["RegisteredMerchants:"+appId+":MerchantReturnUrl"]
                };
            
                HttpContext.Session.SetString("UserId", debitCardTransaction.MerchantUserId);
                HttpContext.Session.SetString("ReturnURL", debitCardTransaction.MerchantReturnURL);
                HttpContext.Session.SetString("TransactionURL",setting["RegisteredMerchants:"+appId+":MerchantTransactionUrl"]);
                HttpContext.Session.SetString("Amount",debitCardTransaction.Amount.ToString());
                HttpContext.Session.SetString("Remarks",debitCardTransaction.Remarks);
                
                #region obtainTokenForNoUse
                string URL = "https://edubankauthentication.azurewebsites.net/api/authentication/";

                var response = httpClient.PostAsJsonAsync(URL,new {UserName="Harsh",Password="EqRUb01Oh5$"}).Result;
                if (response.IsSuccessStatusCode){
                    var  result=response.Content.ReadAsStringAsync().Result;
                    if(!string.IsNullOrEmpty(result)){
                        var serviceUser= JsonConvert.DeserializeObject<Common.Models.UserAuth>(result);
                        HttpContext.Session.SetString("UserToken",serviceUser.Token);
                        }
                }

                #endregion
                ViewData["MaskedCardNumber"] = new string("XXXX XXXX XXXX "+debitCardTransaction.DebitCard.DebitCardNumber.Substring(12,4));
                ViewData["MerchantName"]=debitCardTransaction.MerchantName;
                ModelState.Clear();
                return View(debitCardTransaction.DebitCard);

            }
            catch(Exception ex){
                return RedirectToAction("Error","RedirectToMerchant");
            } 
        }

        [HttpPost]
        public IActionResult DebitCardPayment(DebitCardDetails debitCardDetails)
        {
            try{
                
                if (HttpContext.Session.GetString("UserToken") == null){
                    return RedirectToAction("Failure","RedirectToMerchant");
                }

            var debitCardTransaction = new DebitCardTransaction(){
                DebitCard = debitCardDetails,
                Amount = Convert.ToDecimal(HttpContext.Session.GetString("Amount")),
                Remarks = HttpContext.Session.GetString("Remarks")
            };
            
            // Create a transaction object using debitCardDetails and session amount and remarks
            //string jsonUrlBody = JsonConvert.SerializeObject(new {DebitCard = debitCardTransaction.DebitCard, Amount=debitCardTransaction.Amount, Remarks = debitCardTransaction.Remarks});
            string URL = "https://edubankdebitcardservice.azurewebsites.net/api/paymentgateway/debitcardpayment/";
            //string requestBody = JsonConvert.SerializeObject(debitCardTransaction); //replace with created transaction object

            // Adding Token to header
            //var tokenBytes = Encoding.ASCII.GetBytes(HttpContext.Session.GetString("UserToken"));

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",HttpContext.Session.GetString("UserToken"));

            var response = httpClient.PostAsJsonAsync(URL,new {DebitCard = debitCardTransaction.DebitCard, Amount=debitCardTransaction.Amount, Remarks = debitCardTransaction.Remarks});
            var result = response.Result.Content.ReadAsAsync<string>().Result;
            if (result.Equals("SUCCESS")){

                // var responseForConfirmation = httpClient.PostAsJsonAsync(HttpContext.Session.GetString("TransactionURL"),new{ userid = HttpContext.Session.GetString("UserId"), amount = HttpContext.Session.GetString("Amount")});
                // if (responseForConfirmation.Result.IsSuccessStatusCode)
                    return RedirectToAction("Success","RedirectToMerchant");
                }
            //Update Status to Failed 'F'
            return RedirectToAction("Failure","RedirectToMerchant");

            }
            catch(Exception ex){
                return RedirectToAction("Error","RedirectToMerchant");
            }
        }


        public string DecryptForInputParams(string toDecrypt, Boolean isNum){
            try{
                var specialChars = "1@euTp9!mzXrxP]~tnSl{|4jw5}[";
                //toDecrypt = Microsoft.AspNetCore.WebUtilities.WebEncoders.Base64UrlDecode(toDecrypt).ToString(); // replace url chars
                var decryptedData = string.Empty;
                int position = 0;
                foreach(var item in toDecrypt){
                    position = specialChars.IndexOf(item);
                    if (position>=0 && position<26){
                        if(isNum){
                            decryptedData+=position;
                        }
                        else{
                            decryptedData+= (char)(65 + position);
                        }
                        for(int i=0;i<position;i++){
                            specialChars = specialChars.Substring(1) + specialChars[0];
                        }
                    }
                }
                return decryptedData;
            }
            catch(Exception ex){
                return null;
            }
        }
    }
}

{
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    }
  },
  "AllowedHosts": "*",
  "RegisteredMerchants":{
    "9999":{
      "MerchantName": "Amigo Wallet",
      "MerchantTransactionUrl" : "https://<hostname>:<port>/AmigoWallet/UserTransactionAPI/creditMoneyUsingDebitCard",
      "MerchantReturnUrl": "http://localhost:4200/#/customer/loadMoneyRedirect/"
    }
  }
}

